from typing import Dict

from fastapi import FastAPI

app = FastAPI()


@app.get("/health-check/")
def health_check() -> Dict[str, str]:
    """
    Test docstring.

    Returns:
        message OKS
    """

    # "message": "OK", "message2": "OK", "message3": "OK", "message4": "OK", "message5": "OK", "message6": "OK"
    return {"message": "OK"}


@app.get("/dummy/")
def dummy_endpoint(body: str) -> str:
    """
    Dummy endpoint to test

    Args:
        body (str): A dummy string.

    Returns:
        Same string.
    """

    return body
